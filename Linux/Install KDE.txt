# OS : Linux Mint 20.1 (Cinnamon)

KDE installation :

1. Open terminal and type "sudo add-apt-repository ppa:kubuntu-ppa/backports"
This command will add the repository

2. Type "sudo apt-get update" to update

3. Type "sudo apt-get install kubuntu-desktop kde-plasma-desktop kscreen"
This command will install KDE

4. Accept everything and choose between "lightdm" and "sddm" (I recommend sddm)
If you choose lightdm, you will keep the Cinnamon login screen
If you choose "sddm", you will have the KDE login screen

5. Restart computer, and before typing your password :
- For the Cinnamon login screen, click on the little icon above right of the user name, then choose "Plasma"
- For the KDE login screen, click on "Cinnamon" on the top left of the screen, then choose "Plasma"
But the KDE default login screen is buggy and I recommend you to change it (See step

This is it, enjoy your new KDE !

_______________________________________________________________________________________________________________________

KDE customization :

When KDE is launched, click on the icon at the bottom left of the screen, then type "System configuration" and run it

1. In "Global theme" :
- Choose "Dark Breeze"
- Then click on "Apply" button

2. In "Workspace behavior", choose "Desktop effects" :
- In the "Accessibility" section, activate the first option "Positioning aid"
- In the "Focus" section, activate the third option "Blur the inactives"
- At then end, in "Ornements" section, activate the three options available "Gelatin windows", "Sheet" and "Fall into ruin"
- Then click on "Apply"

3. Again in "Workspace behavior", choose "Lock screen" :
- Choose the "Appearence" tab, then choose your favorit picture and click on "Apply"

4. In "Startup and shutdown", choose "Login screen (SDDM)" :
- Click on "Get new login screens" then download "Chill for Plasma", activate it, and click on "Apply"

5. Again in "Startup and shutdown", choose "Desktop session" :
- In the "On connection" section, choose the third option "Startup with empty session"

6. Again in "Startup and shutdown", choose "Login screen" :
- Click on "Get new login screens", then download "QuarksSplashDark", activate it and click on "Apply"

7. In "Applications", choose "Start indicator" :
- Choose the fourth option "Bouncing cursor", then click on "Apply"

8. In "Input devices", choose "Touchpad", then in the "Touches" section, activate "Touch to click"
Then in the "Scrolling" section, activate both "Two-finger" and "Scroll reverse" options, then click on "Apply"

8. In "Display and screen", choose "Night color", activate it and click on "Apply"

9. Right click on the desktop, choose "Desktop configuration" then choose your favorit picture and click on "Apply"

10. Right click on the desktop, choose "Add graphical components", then drag and drop these components :
- "Hard drive monitoring"
- "Hard drive space usage"
- "Processor monitoring"
- "Network monitoring"

11. Run "Dolphin" app, click on the 3 bars icon on the top right of the app, then choose "Configure Dolphin" :
- In "General" and "Behavior", deactivate the "Show selection indicator"
- In "Start-up", deactivate the "Open new folders in tabs" option
- Then click on "Apply"

12. Run "Kate" app, click on "Configuration", then choose "Configure Kate" :
- In "Apprearance", in the "Advanced" section, check the second option "Highlight the range between the selected parentheses"
- In "Fonts and color", in the "Default schema for Kate" section, choose "Breeze Dark"
- In "Modification", in the "Automatic braces" section, check "Activate automatic braces", and choose the option "<>(){}[]"
- In "Open/Save", deactivate the "Add a new line at then end of the file when saving" option

13. Right click on the top left icon of your screen (First option on the taskbar), then choose "Configure applications launcher"
- In the "General" section, check "Sort alphabetically"
- Then in "Active tabs", drag and drop the items in this order : "Applications, Computer, Favorits, History, Exit"

This is it, enjoy your customed KDE !