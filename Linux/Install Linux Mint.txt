# OS : Linux Mint 20.1 (Cinnamon)

1. Plug in the bootable usb stick, and start your computer with the usb (BIOS settings)

2. Connect internet if not done
! WARNING : If you need to type the Wifi password, be aware that your keyboard is in QWERTY mode !

3. When you are connected to the internet, run "Install Linux Mint" from the desktop

4. Choose your language, then click on "Continue"

5. Choose your keyboard disposition, then click on "Continue"

6. Check "Install multimedia codecs", then click on "Continue"

7. Installation type :
- Choose first option if you want to reinstall the OS
- Choose the second option if you want to replace all your computer with Linux Mint (this will erase ALL your data)
- Choose the third option if you want to do something else (we will choose this option for the tuto)
- Then click on "Continue"

8. Choose the partition where you want to install Linux :
- Choose "ext4" as files system, check "Format partition" and choose "/" as mounting point

9. If you want to have your "Home" folder on another partition
- Choose "ext4" as files system, check "Format partition" and choose "/home" as mounting point

10. Click on "Install now", choose your localisation, then click on "Continue"

11. Choose your name, your computer's name, your user name, your password, then click on "Continue"
! NOTE : Now your keyboard configuration should be good, because you choose your keyboard disposition at step 5 !

12. Wait for installation to complete, then click on "Restart now"

13. Click on the "Bluetooth" icon at the bottom right of the screen and deactivate it

14. Click on the shield icon at the bottom right of the screen, click on "Validate" :
- Click on "Apply update" and enter your password
- On the top you have a message "Would you like to use a local repository mirror ?", click on "Yes" and enter your password
- In "Principal" and "Base", choose the best server (check connection speed on the right), then click on "Validate"
- Click on "Refresh", then when it's done, click on "Install updates", then "Validate", then enter your password
- Now restart your computer

15. Go in "Settings", then "Themes"
- Window borders : Mint-Y-Dark
- Icons : Mint-Y-Dark
- Controls : Mint-Y-Dark
- Mouse pointer : DMZ-Black
- Desktop : Mint-Y-Dark

This is it, now you are ready to create your first Timeshift backup !
