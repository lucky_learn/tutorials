Setup > Fonts > Force Fonts to 96
Then write these 4 lines in /etc/sddm.conf
    [X11]
    ServerArguments=-nolisten tcp -dpi 96 (this will set fonts to 96)
    [XDisplay]
    DisplayCommand=/usr/share/sddm/scripts/Xsetup (this will add this file as command)
And add this line to /usr/share/sddm/scripts/Xsetup
    xrandr --output HDMI-0 --off (This will disable the HDMI-0 display in sddm, change HDMI-0 for the name of your display port)