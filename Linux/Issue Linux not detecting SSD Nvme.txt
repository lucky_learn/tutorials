# OS : Linux Mint 20.1 (live USB), Windows 10
# Issue : Linux doesn't detect my SSD Nvme
# Solution : Change Sata settings in BIOS, and change settings in Windows (if you still want to use this terrible OS lol)

*** NOTE : If you don't have Windows installed on your computer, just follow the point "2." of this tutorial ! ***

1. First we need to configure Windows 10 so it can run with the AHCI mode. So we need to start it in safeboot mode
- Start Windows 10, open cmd in Administrator mode and type "bcdedit /set {current} safeboot minimal" and press enter

2. Reboot your computer and go to the BIOS settings, find the Sata settings and change it for "AHCI"

3. Now we can reboot Windows 10 and deactivate the safeboot mode. Then when shutting down the computer, Windows will install AHCI
- Start Windows 10, open cmd in Administrator mode and type "bcdedit /deletevalue {current} safeboot" and press enter
- Reboot the computer and make sure Windows 10 starts without any problem

This is it, now Linux will detect your SSD Nvme device !