# OS : Linx Mint 20.1

1. Create MANIFEST.MF file in the app directory
- Write this line in the file : " Main-Class: *main_class* "
Replace *main_class* With the name of the ".java" file that contains the "main" method
But remove the ".java"
(For example : "Main-Class: Example")

2. Compile all the ".java" files of your app
(For example : "javac *.java")

Now you can delete all the ".java" files if you want
("rm *.java") will delete all ".java" files in this folder

3. Run this command : "jar cvmf MANIFEST.MF YourApp.jar YourFiles.class"
(For example : "jar cvmf MANIFEST.MF Example.jar *.class")

4. Now you can run the ".jar" file by typing "java -jar YourFile.jar"
(For example : "java -jar Example.jar")

=============================================

2. Compile with packages:
If your packages are "xxx.yyy.zzz1", "xxx.yyy.zzz2", "xxx.yyy.zzz3" etc then

a. Go in the folder where there is the folder "xxx" (if you enter "ls" you should see "xxx")

b. Compile all ".java" files in one command
(For example: "javac xxx/yyy/zzz1/*.java xxx/yyy/zzz2/*.java xxx/yyy/zzz3/*.java")

Now you can delete all the ".java" files of all packages if you want
(find . -name \*.java -type f -delete) will delete all ".java" files in all the folders